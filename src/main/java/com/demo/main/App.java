package com.demo.main;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.AnnotationConfiguration;

import com.demo.model.User;

public class App {
	SessionFactory factory;
	
	App(){
		try {
			factory=new AnnotationConfiguration().configure().buildSessionFactory();
			Session session=factory.openSession();
			Transaction t=session.beginTransaction();
			User user=new User("john","smith",5600);
			session.save(user);
			System.out.println("Saved!");
		t.commit();
		session.close();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
new App();
	}

}
